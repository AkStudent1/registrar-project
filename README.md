# Registrar Portal Web Application #

## README ##

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A simple web application allowing registrar employees to perform a variety of tasks.

In this release of the system the following use cases have been implemented:

1. Adding a course for a given semester

2. Updating/deleting a course for a given semester

3. Searching for courses and students

4. Updating student information


### How do I get set up? ###

In order to use this application:

1. Clone the repository to your machine

2. Ensure you have python 3.4.3 or greater installed

3. Ensure you have the Django framework (v 1.9.6) installed

4. Run django database configuration steps as suggested by the official Django comentation 
([https://docs.djangoproject.com/en/1.10/intro/tutorial02/](https://docs.djangoproject.com/en/1.10/intro/tutorial02/))

5. Run the development server

6. Use a web browser to ensure the application is running

For more information on setting up the Django framework please refer to the following url(s):

[https://docs.djangoproject.com/en/1.10/contents/](https://docs.djangoproject.com/en/1.10/contents/)

### Who do I talk to? ###

Suggestions, comments and errors can be directed to the following email address(es):

abhzkapila999@gmail.com