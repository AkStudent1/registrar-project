from django import forms
from django.db import models
from django.core.exceptions import ValidationError
from .models import Course, Student


class DateInput(forms.DateInput):
    input_type = "date"


class AddCourseForm(forms.ModelForm):

    # def __init__(self, *args, **kwargs):
    #     super(AddCourseForm, self).__init__(*args, **kwargs)
    #     self.fields["course_id"].required = True
    #     for key in self.fields:
    #         self.fields[key].required = True

    class Meta:
        model = Course
        fields = ("course_id", "course_name", "instructor", "semester", "year", "day", "time", "start_date", "end_date",
                  "location", "department", "seats")
        widgets = {
            "course_id": forms.TextInput(attrs={"placeholder": "e.g APT 2080", "required": "true"}),
            "course_name": forms.TextInput(attrs={"placeholder": "e.g Software Engineering", "required": "true"}),
            "instructor": forms.TextInput(attrs={"placeholder": "e.g Dr. Chege", "required": "true"}),
            "year": forms.TextInput(attrs={"placeholder": "e.g 2016", "required": "true"}),
            "location": forms.TextInput(attrs={"placeholder": "e.g Lab 2", "required": "true"}),
            "seats": forms.TextInput(attrs={"required": "true"}),
            "start_date": DateInput(),
            "end_date": DateInput(),
        }


class EditCourseForm(forms.ModelForm):

    class Meta:
        model = Course
        fields = ("course_id", "course_name", "instructor", "semester", "year", "day", "time", "start_date", "end_date",
                  "location", "department", "seats")
        widgets = {
            "course_id": forms.TextInput(attrs={"required": "true"}),
            "course_name": forms.TextInput(attrs={"required": "true"}),
            "instructor": forms.TextInput(attrs={"required": "true"}),
            "year": forms.TextInput(attrs={"required": "true"}),
            "location": forms.TextInput(attrs={"required": "true"}),
            "seats": forms.TextInput(attrs={"min": "0", "max": "100", "required": "true"}),
            "start_date": DateInput(),
            "end_date": DateInput(),
        }


class UpdateStudentForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = (
            "student_id",
            "prefix",
            "first_name",
            "second_name",
            "last_name",
            "dob",
            "gender",
            "ethnicity",
            "email",
            "primary_telephone",
            "secondary_telephone",
            "postal_address",
            "marital_status",
            "kin_name",
            "kin_email",
            "kin_telephone",
            "classification",
            "academic_status",
            "enrolled_date",
            "planned_grad_date",
            "degree",
            "major1",
            "major2",
            "minor1",
            "minor2",
            "conc1",
            "conc2",
            "prev_education",
            "comments"
        )
        # prefix, gender, phone missing from widgets because it changes to text
        widgets = {
            "student_id": forms.TextInput(attrs={"required": "true", "readonly": "true", "disabled": "true"}),
            "first_name": forms.TextInput(attrs={"required": "true", "placeholder": "First Name"}),
            "second_name": forms.TextInput(attrs={"placeholder": "Second Name"}),
            "last_name": forms.TextInput(attrs={"placeholder": "Last Name"}),
            "dob": DateInput(),
            "email": forms.TextInput(attrs={"placeholder": "example@email.com"}),
            "kin_email": forms.TextInput(attrs={"placeholder": "example@email.com"}),
            "enrolled_date": DateInput(),
            "planned_grad_date": DateInput(),
            "major1": forms.TextInput(attrs={"placeholder": "e.g. APT"}),
            "minor1": forms.TextInput(attrs={"placeholder": "e.g. PSY"}),
            "conc1": forms.TextInput(attrs={"placeholder": "e.g. Networking"}),
        }
