from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
# Create your models here.

class Course(models.Model):

    SEMESTERS = (
        ("Spring", "Spring"),
        ("Summer", "Summer"),
        ("Fall", "Fall")
    )

    DAYS = (
        ("Monday & Wednesday", "Monday & Wednesday"),
        ("Tuesday & Thursday", "Tuesday & Thursday"),
        ("Friday", "Friday"),
        ("Saturday", "Saturday")
    )
    # TODO: must do a check so that friday/saturday classes only have 3 hour times
    # and mon-thu don't have fri-sat times
    TIMES = (
        ("0900 to 1040", "0900 to 1040"),
        ("1100 to 1240", "1100 to 1240"),
        ("1320 to 1500", "1320 to 1500"),
        ("1530 to 1710", "1530 to 1710"),
        ("1720 to 1900", "1720 to 1900"),
        ("1920 to 2100", "1920 to 2100"),
        ("1330 to 1700", "1330 to 1700"),
        ("1740 to 2100", "1740 to 2100")
    )

    DEPARTMENTS = (
        ("ACT", "ACT"),
        ("APT", "APT"),
        ("IBA", "IBA"),
        ("IR", "IR"),
        ("IST", "IST"),
        ("PHL", "PHL"),
        ("PSY", "PSY")
    )

    course_id = models.CharField(blank=False, max_length=10)
    course_name = models.CharField(blank=False, max_length=50)
    instructor = models.CharField(blank=False, max_length=50)
    semester = models.CharField(blank=False, max_length=10, choices=SEMESTERS)
    year = models.CharField(blank=False, max_length=4,
                            help_text="Please input a valid year when the course will be offered")
    day = models.CharField(blank=False, max_length=20, choices=DAYS)
    time = models.CharField(blank=False, max_length=15, choices=TIMES)
    start_date = models.DateField(blank=False)
    end_date = models.DateField(blank=False)
    location = models.CharField(blank=False, max_length=30)
    department = models.CharField(blank=False, max_length=7, choices=DEPARTMENTS)
    seats = models.IntegerField(blank=False, default=1, validators=[
        MaxValueValidator(100),
        MinValueValidator(0)])
    deleted = models.BooleanField(blank=False, default=False)

    def __str__(self):
        return "{0} - {1} {2} {3}\n{4} {5}".format(self.course_id, self.course_name, self.semester, self.year, self.day,
                                                   self.time)


class Student(models.Model):
    PREFIXES = (
        ("Mr", "Mr"),
        ("Mrs", "Mrs"),
        ("Miss", "Miss")
    )

    GENDERS = (
        ("Female", "Female"),
        ("Male", "Male")
    )

    MARRIAGE_STATUSES = (
        ("Divorced", "Divorced"),
        ("Married", "Married"),
        ("Single", "Single")
    )

    CLASSIFICATIONS = (
        ("Freshman", "Freshman"),
        ("Sophomore", "Sophomore"),
        ("Junior", "Junior"),
        ("Senior", "Senior"),
        ("Graduated", "Graduated"),
        ("Post Graduate", "Post Graduate"),
        ("Doctorate", "Doctorate"),
        ("Leave", "Leave"),
        ("Other", "Other")
    )

    ACADEMIC_STATUSES = (
        ("Good", "Good"),
        ("Disciplinary Action", "Disciplinary Action"),
        ("Suspended", "Suspended"),
        ("Expelled", "Expelled")
    )

    DEGREES = (
        ("Bachelor of Arts", "Bachelor of Arts"),
        ("Bachelor of Science", "Bachelor of Science")
    )

    student_id = models.IntegerField(blank=False, primary_key=True, validators=[
        MinValueValidator(1),
        MaxValueValidator(30000000)     # 30 million
    ])
    prefix = models.CharField(blank=False, max_length=5, choices=PREFIXES)
    first_name = models.CharField(blank=False, max_length=30)
    second_name = models.CharField(blank=True, null=True, max_length=30)        # possible null saved as '' if null
    last_name = models.CharField(blank=False, max_length=30)
    dob = models.DateField(blank=True, null=True)                        # possible null
    gender = models.CharField(blank=False, max_length=6, choices=GENDERS)
    ethnicity = models.CharField(blank=True, null=True, max_length=35)    # possible null saved as '' if null
    email = models.EmailField(blank=True, null=True)                      # possible null
    primary_telephone = models.IntegerField(blank=True, null=True, validators=[
        RegexValidator(r'\d{10,10}', 'Number must be 10 digits', 'Invalid number')
    ])                                                                  # possible null
    secondary_telephone = models.IntegerField(blank=True, null=True, validators=[
        RegexValidator(r'\d{10,10}', 'Number must be 10 digits', 'Invalid number')
    ])                                                                  # possible null
    postal_address = models.CharField(blank=True, null=True, max_length=75)    # possible null saved as '' if null
    marital_status = models.CharField(blank=False, max_length=10, choices=MARRIAGE_STATUSES)
    kin_name = models.CharField(blank=True, null=False, max_length=50)        # possible null saved as '' if null
    kin_email = models.EmailField(blank=True, null=True)                      # possible null
    kin_telephone = models.IntegerField(blank=True, null=True, validators=[
        RegexValidator(r'\d{10,10}', 'Number must be 10 digits', 'Invalid number')
    ])                                                                  # possible null
    classification = models.CharField(blank=False, max_length=15, choices=CLASSIFICATIONS)
    academic_status = models.CharField(blank=False, max_length=20, choices=ACADEMIC_STATUSES)
    enrolled_date = models.DateField(blank=False)
    planned_grad_date = models.DateField(blank=False)
    degree = models.CharField(blank=False, max_length=40, choices=DEGREES)
    major1 = models.CharField(blank=True, null=True, max_length=50)     # possible null saved as '' if null
    major2 = models.CharField(blank=True, null=True, max_length=50)     # possible null saved as '' if null
    minor1 = models.CharField(blank=True, null=True, max_length=50)     # possible null saved as '' if null
    minor2 = models.CharField(blank=True, null=True, max_length=50)     # possible null saved as '' if null
    conc1 = models.CharField(blank=True, null=True, max_length=50)     # possible null saved as '' if null
    conc2 = models.CharField(blank=True, null=True, max_length=50)     # possible null saved as '' if null
    prev_education = models.CharField(blank=False, max_length=100)
    comments = models.TextField(blank=True, null=True, max_length=600)  # possible null saved as '' if null

    def __str__(self):
        return "{0} - {1} {2} {3}".format(self.student_id, self.first_name, self.second_name, self.last_name)
