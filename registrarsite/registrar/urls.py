from django.conf.urls import url
from . import views

app_name = "registrar"

urlpatterns = [
    # Home page (for log in)
    url(r'^$', views.index, name="home"),
    # POST form to log in users
    url(r'^sign-in/$', views.signin, name="signin"),
    # Dashboard for logged in users
    url(r'^dashboard/?$', views.dashboard, name="dashboard"),
    # Add course page
    url(r'^add-course/?$', views.add_course, name="add_course"),
    # POST form to save course
    url(r'^save-course/?$', views.save_course, name="save_course"),
    # Drop course page
    url(r'^view-course/?$', views.view_course, name="view_course"),
    # Course details page
    url(r'^update-course/(?P<cid>\d+)/?$', views.update_course, name="update_course"),
    # POST form to update course
    url(r'^edit-course/(?P<cid>\d+)/?$', views.edit_course, name="edit_course"),
    # Delete Course
    url(r'^delete_course/(?P<cid>\d+)/?$', views.delete_course, name="delete_course"),
    # View Student Page
    url(r'^view-student/?$', views.view_student, name="view_student"),
    # Update Student Page
    url(r'^update-student/(?P<sid>\d+)/?$', views.update_student, name="update_student"),
    # POST form to edit student information
    url(r'^edit-student/(?P<sid>\d+)/?$', views.edit_student, name="edit_student"),
    # Log out link
    url(r'^log-out/$', views.log_out, name="log_out")
]
