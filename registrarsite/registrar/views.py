from django.shortcuts import render, redirect, get_list_or_404, get_object_or_404, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django import forms        # will be used in Post
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from .models import Course, Student
from .forms import AddCourseForm, EditCourseForm, UpdateStudentForm

# Create your views here.


def index(request):
    '''
    Args:
        request: requested URL - has lots of information including cookie information
    Returns: HTTP Response Object

    Directs anonymous users to the Home page (log in page).
    Logged in users should be directed to the dashboard
    '''
    if request.user.is_authenticated():
        return HttpResponseRedirect("/dashboard/")
    return render(request, "registrar/index.html", {})


def signin(request):
    '''
    Args:
        request: requested URL - has lots of information including cookie information
    Returns: HTTP Response Object

    Is a POST form that is linked by the form in index.html.
    Takes provided username and password and attempts to authenticate.
    Authentication failure, NOT POST and Banned users results in error message and redirect to index
    Successful authentication takes users to their dashboard
    NB: Must use HttpResponseRedirect to avoid double POST if user clicks back
    '''
    if request.method != "POST":
        messages.error(request, "There was a problem submitting your form. Try again")
        return HttpResponseRedirect("/")
    try:
        username = request.POST["username"]
        password = request.POST["password"]
    except KeyError:
        messages.error(request, "There was an error retrieving your data. Please try again")
        return HttpResponseRedirect("/")
    user = authenticate(username=username, password=password)
    if user is not None:    # User exists
        if user.is_active:  # User not banned/blocked
            login(request, user)
            messages.success(request, "Successfully Logged In")
            return HttpResponseRedirect("/dashboard/")
        else:
            messages.warning(request, "You have been banned")
            return HttpResponseRedirect("/")
    messages.error(request, "Invalid username or password")
    return HttpResponseRedirect("/")


def dashboard(request):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
        return HttpResponseRedirect("/")
    return render(request, "registrar/dashboard.html")


def add_course(request):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
        return HttpResponseRedirect("/")
    # TODO: delete all previous courses
    form = AddCourseForm()
    return render(request, "registrar/add-course.html", {"form": form})


def save_course(request):
    if request.method != "POST":
        messages.error(request, "There was a problem submitting your form. Try again")
        return HttpResponseRedirect("/dashboard/")
    form = AddCourseForm(request.POST)
    if form.is_valid():
        # commit=False means the form doesn't save at this time.
        # commit defaults to True which means it normally saves.
        if Course.objects.filter(course_id=request.POST["course_id"], time=request.POST["time"],
                                 semester=request.POST["semester"], year=request.POST["year"], deleted=False).exists():
            messages.error(request, "A course with that same course code already exists for the specified time and "
                                    "semester")
            return HttpResponseRedirect("/add-course/")
        course = form.save()
        return HttpResponseRedirect("/update-course/{0}".format(course.id))
    messages.error(request, "The form was not fully/correctly filled. Please try again.")
    return HttpResponseRedirect("/add-course/")


def view_course(request):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
        return HttpResponseRedirect("/")
    if request.method != "POST":
        return render(request, "registrar/view-course.html", {})
    # Get POST data
    try:
        semester = request.POST["semester"]
        year = request.POST["year"]
        ccode = request.POST["course-code"]
    except KeyError:
        messages.error(request, "There was a problem retrieving information. Please try again")
        return HttpResponseRedirect("/dashboard/")
    include_deleted = None
    try:
        include_deleted = bool(request.POST["include_deleted"])
    except KeyError:
        include_deleted = False
    # Return results
    try:
        if ccode != "":
            if include_deleted:
                results = Course.objects.filter(semester=semester, year=year, course_id=ccode)
                if len(results) == 0:
                    messages.info(request, "No courses found")
                    return HttpResponseRedirect("/view-course/")
                return render(request, "registrar/view-course.html", {"results": results})
            else:
                results = Course.objects.filter(semester=semester, year=year, course_id=ccode, deleted=False)
                if len(results) == 0:
                    messages.info(request, "No courses found")
                    return HttpResponseRedirect("/view-course/")
                return render(request, "registrar/view-course.html", {"results": results})
        else:
            if include_deleted:
                results = Course.objects.filter(semester=semester, year=year)
                if len(results) == 0:
                    messages.info(request, "No courses found")
                    return HttpResponseRedirect("/view-course/")
                return render(request, "registrar/view-course.html", {"results": results})
            else:
                results = Course.objects.filter(semester=semester, year=year, deleted=False)
                if len(results) == 0:
                    messages.info(request, "No courses found")
                    return HttpResponseRedirect("/view-course/")
                return render(request, "registrar/view-course.html", {"results": results})
    except Course.DoesNotExist:
        messages.info(request, "No courses found")
        return HttpResponseRedirect("/view-course/")


def update_course(request, cid):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
        return HttpResponseRedirect("/")
    course = get_object_or_404(Course, id=cid)
    form = EditCourseForm(initial={
        "course_id": course.course_id,
        "course_name": course.course_name,
        "instructor": course.instructor,
        "semester": course.semester,
        "year": course.year,
        "day": course.day,
        "time": course.time,
        "start_date": course.start_date,
        "end_date": course.end_date,
        "location": course.location,
        "department": course.department,
        "seats": course.seats
    })
    return render(request, "registrar/update-course.html", {"form": form, "id": cid})


def edit_course(request, cid):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
    if request.method != "POST":
        messages.error(request, "There was a problem submitting your form. Try again")
        return HttpResponseRedirect("/dashboard/")
    form = EditCourseForm(request.POST)
    if not form.is_valid():
        messages.error(request, "The form was filled incorrectly. Please try again")
        return HttpResponseRedirect("/update-course/{0}".format(cid))
    try:
        course = Course.objects.get(id=cid)
    except Course.DoesNotExist:
        messages.error(request, "Could not find the course to update.")
        return HttpResponseRedirect("/update-course/{0}".format(cid))

    try:
        course.course_id = request.POST["course_id"]
        course.course_name = request.POST["course_name"]
        course.instructor = request.POST["instructor"]
        course.semester = request.POST["semester"]
        course.year = request.POST["year"]
        course.day = request.POST["day"]
        course.time = request.POST["time"]
        course.start_date = request.POST["start_date"]
        course.end_date = request.POST["end_date"]
        course.location = request.POST["location"]
        course.department = request.POST["department"]
        course.seats = request.POST["seats"]
        course.save()
        messages.success(request, "Course was successfully updated")
        return HttpResponseRedirect("/update-course/{0}".format(cid))
    except KeyError:
        messages.error(request, "Could not retrieve form data. Please try again")
        HttpResponseRedirect("/update-course/{0}".format(cid))
    messages.error(request, "The form was not fully/correctly filled. Please try again.")
    return HttpResponseRedirect("/update-course/{0}".format(cid))


def delete_course(request, cid):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
    try:
        course = Course.objects.get(id=cid)
        if course.deleted:
            messages.info(request, "Course has already been deleted")
            HttpResponseRedirect("/update-course/{0}".format(cid))
        course.deleted = True
        course.save()
        messages.success(request, "The course was successfully deleted")
        return HttpResponseRedirect("/dashboard/")
    except Course.DoesNotExist:
        messages.error(request, "The course could not be deleted")
        return HttpResponseRedirect("/dashboard/")


def view_student(request):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
        return HttpResponseRedirect("/")
    if request.method != "POST":
        # Return normal page to search for student
        return render(request, "registrar/view-student.html", {})
    # Get POST data
    try:
        student_id = request.POST["student_id"]
    except KeyError:
        messages.error(request, "There was a problem retrieving information. Please try again")
        return HttpResponseRedirect("/dashboard/")
    # Take user to student page or show not found message
    try:
        result = Student.objects.get(student_id=student_id)
        return HttpResponseRedirect("/update-student/{0}".format(student_id))
    except Student.DoesNotExist:    # Student can't be found
        messages.info(request, "No Students found")
        return HttpResponseRedirect("/view-student/")


def update_student(request, sid):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
        return HttpResponseRedirect("/")
    student = get_object_or_404(Student, student_id=sid)
    form = UpdateStudentForm(initial={
        "student_id": student.student_id,
        "first_name": student.first_name,
        "second_name": student.second_name,
        "last_name": student.last_name,
        "dob": student.dob,
        "gender": student.gender,
        "ethnicity": student.ethnicity,
        "marital_status": student.marital_status,
        "email": student.email,
        "primary_telephone": student.primary_telephone,
        "secondary_telephone": student.secondary_telephone,
        "postal_address": student.postal_address,
        "classification": student.classification,
        "academic_status": student.academic_status,
        "enrolled_date": student.enrolled_date,
        "planned_grad_date": student.planned_grad_date,
        "degree": student.degree,
        "major1": student.major1,
        "major2": student.major2,
        "minor1": student.minor1,
        "minor2": student.minor2,
        "conc1": student.conc1,
        "conc2": student.conc2,
        "prev_education": student.prev_education,
        "kin_name": student.kin_name,
        "kin_email": student.kin_email,
        "kin_telephone": student.kin_telephone,
        "comments": student.comments
    })
    return render(request, "registrar/update-student.html", {"form": form, "id": sid})


def edit_student(request, sid):
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
    if request.method != "POST":
        messages.error(request, "There was a problem submitting your form. Try again")
        return HttpResponseRedirect("/dashboard/")
    # Update Logic
    try:
        stud = Student.objects.get(student_id=sid)
    except Student.DoesNotExist:
        messages.error(request, "Could not find the student to update")
        return HttpResponseRedirect("/update-student/{0}".format(sid))
    # Update fields
    try:
        stud.prefix = request.POST["prefix"]
        stud.first_name = request.POST["first_name"]
        stud.second_name = request.POST["second_name"]
        stud.last_name = request.POST["last_name"]
        if request.POST["dob"] != "":
            stud.dob = request.POST["dob"]
        stud.gender = request.POST["gender"]
        stud.ethnicity = request.POST["ethnicity"]
        stud.email = request.POST["email"]
        if request.POST["primary_telephone"] != "":
            stud.primary_telephone = request.POST["primary_telephone"]
        if request.POST["secondary_telephone"] != "":
            stud.secondary_telephone = request.POST["secondary_telephone"]
        stud.postal_address = request.POST["postal_address"]
        stud.marital_status = request.POST["marital_status"]
        stud.kin_name = request.POST["kin_name"]
        stud.kin_email = request.POST["kin_email"]
        if request.POST["kin_telephone"] != "":
            stud.kin_telephone = request.POST["kin_telephone"]
        stud.classification = request.POST["classification"]
        stud.academic_status = request.POST["academic_status"]
        if request.POST["enrolled_date"] != "":
            stud.enrolled_date = request.POST["enrolled_date"]
        if request.POST["planned_grad_date"] != "":
            stud.planned_grad_date = request.POST["planned_grad_date"]
        stud.degree = request.POST["degree"]
        stud.major1 = request.POST["major1"]
        stud.major2 = request.POST["major2"]
        stud.minor1 = request.POST["minor1"]
        stud.minor2 = request.POST["minor2"]
        stud.conc1 = request.POST["conc1"]
        stud.conc2 = request.POST["conc2"]
        stud.prev_education = request.POST["prev_education"]
        stud.comments = request.POST["comments"]
        stud.save()
        messages.success(request, "Student information successfully updated")
        return HttpResponseRedirect("/update-student/{0}".format(sid))
    except KeyError:
        messages.error(request, "Could not retrieve form data. Please try again")
        return HttpResponseRedirect("/update-student/{0}".format(sid))


def log_out(request):
    '''
    Args:
        request: requested URL - has lots of information including cookie information
    Returns: HTTP Response Object

    Logs out a user if the user is logged in.
    Displays a success message and redirects to the home page i.e index
    '''
    if not request.user.is_authenticated():
        messages.error(request, "You are not logged in")
        return HttpResponseRedirect("/")
    logout(request)
    messages.success(request, "Successfully Logged Out")
    return HttpResponseRedirect("/")